// miniprogram/pages/Insights/PostInsights.js
Page({

  /**
   * Page initial data
   */
  data: {

  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {


  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  },
  addInsights : function(event){

    wx.cloud.callFunction({
      name: 'postInsights',
      data: {
        'insights':event.detail.value.insight,
        'description':event.detail.value.description
      },
      success: res => {
        console.log('success ', res);  
      wx.navigateTo({
        url: '../index/index',
      });
      },
      fail: err => {
        console.error('Error while fetching', err)
      
      }
    })

    console.log(" in cloud data");
 },

})